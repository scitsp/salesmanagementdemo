namespace SalesManagementDemo.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SalesPersons",
                c => new
                    {
                        SalesPersonId = c.Int(nullable: false, identity: true),
                        SalesPersonName = c.String(),
                        Address = c.String(),
                        Mobile = c.String(),
                        Email = c.String(),
                    })
                .PrimaryKey(t => t.SalesPersonId);
            
            CreateTable(
                "dbo.ShopDetails",
                c => new
                    {
                        ShopId = c.Int(nullable: false, identity: true),
                        ShopName = c.String(),
                        ShopAddress = c.String(),
                        PhonNo = c.String(),
                    })
                .PrimaryKey(t => t.ShopId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.ShopDetails");
            DropTable("dbo.SalesPersons");
        }
    }
}
