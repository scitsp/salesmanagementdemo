﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SalesManagementDemo.Startup))]
namespace SalesManagementDemo
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
