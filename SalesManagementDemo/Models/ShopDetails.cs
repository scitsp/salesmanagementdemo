﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SalesManagementDemo.Models
{
    public class ShopDetails
    {
        [Key]
        public int ShopId { get; set; }
        public string  ShopName { get; set; }
        public string ShopAddress { get; set; }
        public string PhonNo { get; set; }
        public bool IsSelected { get; set; }
        public Nullable<double> Latitude { get; set; }
        public Nullable<double> Longitude { get; set; }
    }
}