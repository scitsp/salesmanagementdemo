﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SalesManagementDemo.Models
{
   // [Table("SalesPersons")]
    public class SalesPerson
    {
        [Key]
        public int SalesPersonId { get; set; }
        public string SalesPersonName { get; set; }
        public string  Address { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
       
    }
}