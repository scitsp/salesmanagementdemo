﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SalesManagementDemo.Models
{
    public class RoutDetails
    {
        [Key]
        public int RoutId { get; set; }
        public int SalesPersonId { get; set; }
        public int[]  ShopList { get; set; }
        public int ShopId { get; set; }
    }
}