﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SalesManagementDemo.Models
{
    public class RoutDetailsByPerson
    {
        public string SalesPersonName { get; set; }
        //public string[] shopList { get; set; }
        public List<string> shopList { get; set; }
    }
}