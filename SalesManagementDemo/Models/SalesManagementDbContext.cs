﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
namespace SalesManagementDemo.Models
{
    public class SalesManagementDbContext: DbContext
    {
        public SalesManagementDbContext() : base ("myConnectionString"){}

        public DbSet<SalesPerson> SalesPerson { get; set; }
        public DbSet<ShopDetails> ShopDetails { get; set; }
        public DbSet<RoutDetails> RoutDetails { get; set; }
    }
}