﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SalesManagementDemo.Models;

namespace SalesManagementDemo.Controllers
{
    public class ShowRoutController : Controller
    {
        // GET: ShowRout
        SalesManagementDbContext db = new SalesManagementDbContext();
        public ActionResult Index()
        {
            List<SalesPerson> spersonList = db.SalesPerson.ToList();
            ViewBag.SalesPerson = spersonList;
            List<ShopDetails> shpList = db.ShopDetails.ToList();
            ViewBag.ShopDetails = shpList;

            return View();
        }
        [HttpPost]
        public ActionResult ShowRoutDetails(int[] shopIds)
        {

            //int sid = 3;// Convert.ToInt32(id);
            List<ShopDetails> shopList = db.ShopDetails.ToList();

            List<ShopDetails> returnShopList = new List<ShopDetails>();
            if(shopIds != null)
            { 
            foreach (int sid in shopIds)
            { 
             var result = (from sp in shopList where sp.ShopId == sid select sp).ToList();
             foreach(var item in result)
                {
                    ShopDetails shop = new ShopDetails();
                    shop.ShopId = item.ShopId;
                    shop.ShopName = item.ShopName;
                    shop.ShopAddress = item.ShopAddress;
                    shop.Longitude = item.Longitude;
                    shop.Latitude = item.Latitude;
                    returnShopList.Add(shop);
                }
           }
            }
            return Json(returnShopList, JsonRequestBehavior.AllowGet);
        }
    }
}