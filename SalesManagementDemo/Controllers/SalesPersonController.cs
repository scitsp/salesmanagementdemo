﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SalesManagementDemo.Models;

namespace SalesManagementDemo.Controllers
{
    public class SalesPersonController : Controller
    {
        // GET: SalesPerson
        SalesManagementDbContext db = new SalesManagementDbContext();
        public ActionResult Index()
        {
            List<SalesPerson> listSalePer = db.SalesPerson.ToList();
            return View(listSalePer);
        }
        public ActionResult Create()
        {

            return View();
        }
        [HttpPost]
        public ActionResult Create (SalesPerson salpObj)
        {
            db.SalesPerson.Add(salpObj);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id)
        {
            SalesPerson spObj = db.SalesPerson.Find(id);
            db.SalesPerson.Remove(spObj);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        public ActionResult Edit(int id)
        {
            SalesPerson spObj = db.SalesPerson.Find(id);
            
            return View(spObj);
        }
        [HttpPost]
        public ActionResult Edit(SalesPerson spObj)
        {
            db.Entry(spObj).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

    }
}