﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SalesManagementDemo.Models;

namespace SalesManagementDemo.Controllers
{
    public class ShopController : Controller
    {
        SalesManagementDbContext db = new SalesManagementDbContext();
        // GET: Shop
        public ActionResult Index()
        {
            List<ShopDetails> shopList = db.ShopDetails.ToList();
            return View(shopList);
        }
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(ShopDetails shopObj)
        {
            db.ShopDetails.Add(shopObj);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult Edit(int id)
        {
            ShopDetails sopObj = db.ShopDetails.Find(id);
            return View(sopObj);
        }
        [HttpPost]
        public ActionResult Edit(ShopDetails shopObj)
        {
            db.Entry(shopObj).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id)
        {
            ShopDetails spObj = db.ShopDetails.Find(id);
            db.ShopDetails.Remove(spObj);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}