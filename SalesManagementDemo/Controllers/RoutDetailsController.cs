﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SalesManagementDemo.Models;

namespace SalesManagementDemo.Controllers
{
    public class RoutDetailsController : Controller
    {
        SalesManagementDbContext db = new SalesManagementDbContext();
        // GET: RoutDetails
        public ActionResult Index()
        {
            List<SalesPerson> spersonList = db.SalesPerson.ToList();
            ViewBag.SalesPerson = spersonList;
            List<ShopDetails> shpList = db.ShopDetails.ToList();
            ViewBag.ShopDetails = shpList;

            List<RoutDetails> routList = db.RoutDetails.ToList();
            var routDetails = from r in routList
                              join sp in spersonList on r.SalesPersonId equals sp.SalesPersonId
                              join s in shpList on r.ShopId equals s.ShopId 
                             // group r by r.SalesPersonId into g
                              select new
                              {
                                  SalesPersonName = sp.SalesPersonName,
                                  Mobile = sp.Mobile,
                                  ShopName = s.ShopName
                              };
            var listbyPurson = (from p in routDetails
                               group p.ShopName by p.SalesPersonName into g
                               select new { SalesPersonName = g.Key, ShopName = g.ToList() }).ToList();
            //ViewBag.listbyPurson = listbyPurson;
            List<RoutDetailsByPerson> listRout = new List<RoutDetailsByPerson>();
            foreach (var item in listbyPurson)
            {
                RoutDetailsByPerson obj = new RoutDetailsByPerson();
                obj.SalesPersonName = item.SalesPersonName;
                obj.shopList = new List<string>();
                foreach (var itm in item.ShopName)
                {
                    obj.shopList.Add(itm);
                }
                listRout.Add(obj);
            }

            ViewBag.SalesPersonDetails = listRout;

            return View();
        }
        [HttpPost]
        // public ActionResult Index(FormCollection data)
        public ActionResult Index(RoutDetails data)
        {

           foreach(int sid in data.ShopList )
            {
                RoutDetails rouObj = new RoutDetails();
                rouObj.SalesPersonId = data.SalesPersonId;
                rouObj.ShopId = sid;
                db.RoutDetails.Add(rouObj);
                db.SaveChanges();
            } 
            return RedirectToAction("Index");
        }
        }
}